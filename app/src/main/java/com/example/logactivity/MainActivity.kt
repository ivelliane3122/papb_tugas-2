package com.example.logactivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button = findViewById<Button>(R.id.login)
        button.setOnClickListener {

            val inputUsername = findViewById<EditText>(R.id.inputUsername)
            val inputPassword = findViewById<EditText>(R.id.inputPassword)

            val username = inputUsername.text.toString()
            val password = inputPassword.text.toString()

            if (isValidLogin(username, password)) {
                val intent = Intent(this, mrheadActivity::class.java)
                intent.putExtra("username", username)
                intent.putExtra("password", password)
                startActivity(intent)
            } else {
                // Display an error message for invalid login
                Toast.makeText(this, "Invalid username or password", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun isValidLogin(username: String, password: String): Boolean {
        // Define the valid username and password combinations
        return (username == "Merry" && password == "ivel") ||
                (username == "Ivel" && password == "merry")
    }
}