package com.example.logactivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

public class mrheadActivity extends Activity {

    private ImageView imHair, imMoustache, imEyebrow, imBeard;
    private CheckBox cbHair, cbMoustache, cbEyebrow, cbBeard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mrhead);

        imHair = findViewById(R.id.imgHair);
        imMoustache = findViewById(R.id.imgMoustache);
        imEyebrow = findViewById(R.id.imgEyebrow);
        imBeard = findViewById(R.id.imgBeard);

        cbHair = findViewById(R.id.ckHair);
        cbMoustache = findViewById(R.id.ckMoustache);
        cbEyebrow = findViewById(R.id.ckEyebrow);
        cbBeard = findViewById(R.id.ckBeard);

        cbHair.setOnClickListener(v->{
            if (cbHair.isChecked()) {
                imHair.setVisibility(View.VISIBLE);
            }
            else {
                imHair.setVisibility(View.INVISIBLE);
            }
        });

        cbMoustache.setOnClickListener(v->{
            if (cbMoustache.isChecked()) {
                imMoustache.setVisibility(View.VISIBLE);
            }
            else {
                imMoustache.setVisibility(View.INVISIBLE);
            }
        });

        cbEyebrow.setOnClickListener(v->{
            if (cbEyebrow.isChecked()) {
                imEyebrow.setVisibility(View.VISIBLE);
            }
            else {
                imEyebrow.setVisibility(View.INVISIBLE);
            }
        });

        cbBeard.setOnClickListener(v->{
            if (cbBeard.isChecked()) {
                imBeard.setVisibility(View.VISIBLE);
            }
            else {
                imBeard.setVisibility(View.INVISIBLE);
            }
        });
    }
}

